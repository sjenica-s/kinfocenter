msgid ""
msgstr ""
"Project-Id-Version: kinfocenter\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2022-03-05 00:45+0000\n"
"PO-Revision-Date: 2022-03-06 21:34-0800\n"
"Last-Translator: Japanese KDE translation team <kde-jp@kde.org>\n"
"Language-Team: Japanese <kde-jp@kde.org>\n"
"Language: ja\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Accelerator-Marker: &\n"
"X-Text-Markup: kde4\n"

#, kde-format
msgctxt "NAME OF TRANSLATORS"
msgid "Your names"
msgstr ""

#, kde-format
msgctxt "EMAIL OF TRANSLATORS"
msgid "Your emails"
msgstr ""

#: main.cpp:22
#, kde-format
msgctxt "@label kcm name"
msgid "Firmware Security"
msgstr ""

#: main.cpp:26
#, kde-format
msgid "Harald Sitter"
msgstr ""

#: package/contents/ui/main.qml:14
msgctxt "@info:whatsthis"
msgid "Firmware Security Information"
msgstr ""
