# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
# Federico Zenith <federico.zenith@member.fsf.org>, 2010, 2012, 2015.
# Paolo Zamponi <zapaolo@email.it>, 2016, 2018, 2020, 2022.
#
msgid ""
msgstr ""
"Project-Id-Version: kcmdevinfo\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2022-10-16 00:48+0000\n"
"PO-Revision-Date: 2022-09-17 19:09+0200\n"
"Last-Translator: Paolo Zamponi <zapaolo@email.it>\n"
"Language-Team: Italian <kde-i18n-it@kde.org>\n"
"Language: it\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Lokalize 22.08.1\n"

#, kde-format
msgctxt "NAME OF TRANSLATORS"
msgid "Your names"
msgstr "Federico Zenith"

#, kde-format
msgctxt "EMAIL OF TRANSLATORS"
msgid "Your emails"
msgstr "federico.zenith@member.fsf.org"

#: devicelisting.cpp:41
#, kde-format
msgctxt "Device Listing Whats This"
msgid "Shows all the devices that are currently listed."
msgstr "Mostra tutti i dispositivi attualmente elencati."

#: devicelisting.cpp:44
#, kde-format
msgid "Devices"
msgstr "Dispositivi"

#: devicelisting.cpp:57
#, kde-format
msgid "Collapse All"
msgstr "Contrai tutto"

#: devicelisting.cpp:60
#, kde-format
msgid "Expand All"
msgstr "Espandi tutto"

#: devicelisting.cpp:63
#, kde-format
msgid "Show All Devices"
msgstr "Mostra tutti i dispositivi"

#: devicelisting.cpp:66
#, kde-format
msgid "Show Relevant Devices"
msgstr "Mostra i dispositivi rilevanti"

#: devicelisting.cpp:95
#, kde-format
msgctxt "unknown device type"
msgid "Unknown"
msgstr "Sconosciuto"

#: devicelisting.cpp:136 devinfo.cpp:79
#, kde-format
msgctxt "no device UDI"
msgid "None"
msgstr "Nessuno"

#: devinfo.cpp:28
#, kde-format
msgid "kcmdevinfo"
msgstr "kcmdevinfo"

#: devinfo.cpp:28
#, kde-format
msgid "Device Viewer"
msgstr "Visore di dispositivi"

#: devinfo.cpp:28
#, kde-format
msgid "(c) 2010 David Hubner"
msgstr "© 2010 David Hubner"

#: devinfo.cpp:58
#, kde-format
msgid "UDI: "
msgstr "UDI: "

#: devinfo.cpp:66
#, kde-format
msgctxt "Udi Whats This"
msgid "Shows the current device's UDI (Unique Device Identifier)"
msgstr ""
"Mostra l'UDI (identificativo univoco di dispositivo) del dispositivo attuale"

#: infopanel.cpp:20
#, kde-format
msgid "Device Information"
msgstr "Informazioni sul dispositivo"

#: infopanel.cpp:29
#, kde-format
msgctxt "Info Panel Whats This"
msgid "Shows information about the currently selected device."
msgstr "Mostra informazioni sul dispositivo attualmente selezionato."

#: infopanel.cpp:56
#, kde-format
msgid ""
"\n"
"Solid Based Device Viewer Module"
msgstr ""
"\n"
"Modulo del visore di dispositivi basato su Solid"

#: infopanel.cpp:119
#, kde-format
msgid "Description: "
msgstr "Descrizione: "

#: infopanel.cpp:121
#, kde-format
msgid "Product: "
msgstr "Prodotto: "

#: infopanel.cpp:123
#, kde-format
msgid "Vendor: "
msgstr "Fornitore: "

#: infopanel.cpp:145
#, kde-format
msgid "Yes"
msgstr "Sì"

#: infopanel.cpp:147
#, kde-format
msgid "No"
msgstr "No"

#: infopanel.h:36
#, kde-format
msgctxt "name of something is not known"
msgid "Unknown"
msgstr "Sconosciuto"

#: soldevice.cpp:65
#, kde-format
msgctxt "unknown device"
msgid "Unknown"
msgstr "Sconosciuto"

#: soldevice.cpp:91
#, kde-format
msgctxt "Default device tooltip"
msgid "A Device"
msgstr "Un dispositivo"

#: soldevicetypes.cpp:43
#, kde-format
msgid "Processors"
msgstr "Processori"

#: soldevicetypes.cpp:59
#, kde-format
msgid "Processor %1"
msgstr "Processore %1"

#: soldevicetypes.cpp:76
#, kde-format
msgid "Intel MMX"
msgstr "Intel MMX"

#: soldevicetypes.cpp:79
#, kde-format
msgid "Intel SSE"
msgstr "Intel SSE"

#: soldevicetypes.cpp:82
#, kde-format
msgid "Intel SSE2"
msgstr "Intel SSE2"

#: soldevicetypes.cpp:85
#, kde-format
msgid "Intel SSE3"
msgstr "Intel SSE3"

#: soldevicetypes.cpp:88
#, kde-format
msgid "Intel SSSE3"
msgstr "Intel SSSE3"

#: soldevicetypes.cpp:91
#, kde-format
msgid "Intel SSE4.1"
msgstr "Intel SSE4.1"

#: soldevicetypes.cpp:94
#, kde-format
msgid "Intel SSE4.2"
msgstr "Intel SSE4.2"

#: soldevicetypes.cpp:97
#, kde-format
msgid "AMD 3DNow!"
msgstr "AMD 3DNow!"

#: soldevicetypes.cpp:100
#, kde-format
msgid "ATI IVEC"
msgstr "ATI IVEC"

#: soldevicetypes.cpp:103
#, kde-format
msgctxt "no instruction set extensions"
msgid "None"
msgstr "Nessuno"

#: soldevicetypes.cpp:106
#, kde-format
msgid "Processor Number: "
msgstr "Numero del processore: "

#: soldevicetypes.cpp:106
#, kde-format
msgid "Max Speed: "
msgstr "Velocità massima: "

#: soldevicetypes.cpp:107
#, kde-format
msgid "Supported Instruction Sets: "
msgstr "Insiemi di istruzioni supportati: "

#: soldevicetypes.cpp:132
#, kde-format
msgid "Storage Drives"
msgstr "Unità di memorizzazione"

#: soldevicetypes.cpp:151
#, kde-format
msgid "Hard Disk Drive"
msgstr "Disco fisso"

# CompactFlash è un nome proprio.
# http://it.wikipedia.org/wiki/CompactFlash
#: soldevicetypes.cpp:154
#, kde-format
msgid "Compact Flash Reader"
msgstr "Lettore CompactFlash"

#: soldevicetypes.cpp:157
#, kde-format
msgid "Smart Media Reader"
msgstr "Lettore SmartMedia"

#: soldevicetypes.cpp:160
#, kde-format
msgid "SD/MMC Reader"
msgstr "Lettore SD/MMC"

#: soldevicetypes.cpp:163
#, kde-format
msgid "Optical Drive"
msgstr "Unità ottica"

# Non le generiche penne di memoria USB, ma uno standard specifico della Sony.
# http://it.wikipedia.org/wiki/Memory_Stick
#: soldevicetypes.cpp:166
#, kde-format
msgid "Memory Stick Reader"
msgstr "Lettore Memory Stick"

#: soldevicetypes.cpp:169
#, kde-format
msgid "xD Reader"
msgstr "Lettore xD"

#: soldevicetypes.cpp:172
#, kde-format
msgid "Unknown Drive"
msgstr "Unità sconosciuta"

#: soldevicetypes.cpp:192
#, kde-format
msgid "IDE"
msgstr "IDE"

#: soldevicetypes.cpp:195
#, kde-format
msgid "USB"
msgstr "USB"

#: soldevicetypes.cpp:198
#, kde-format
msgid "IEEE1394"
msgstr "IEEE1394"

#: soldevicetypes.cpp:201
#, kde-format
msgid "SCSI"
msgstr "SCSI"

#: soldevicetypes.cpp:204
#, kde-format
msgid "SATA"
msgstr "SATA"

#: soldevicetypes.cpp:207
#, kde-format
msgctxt "platform storage bus"
msgid "Platform"
msgstr "Piattaforma"

#: soldevicetypes.cpp:210
#, kde-format
msgctxt "unknown storage bus"
msgid "Unknown"
msgstr "Sconosciuta"

#: soldevicetypes.cpp:213
#, kde-format
msgid "Bus: "
msgstr "Bus: "

#: soldevicetypes.cpp:213
#, kde-format
msgid "Hotpluggable?"
msgstr "Collegabile a caldo?"

#: soldevicetypes.cpp:213
#, kde-format
msgid "Removable?"
msgstr "Rimovibile?"

#: soldevicetypes.cpp:257
#, kde-format
msgid "Unused"
msgstr "Inutilizzata"

#: soldevicetypes.cpp:260
#, kde-format
msgid "File System"
msgstr "File system"

#: soldevicetypes.cpp:263
#, kde-format
msgid "Partition Table"
msgstr "Tabella delle partizioni"

#: soldevicetypes.cpp:266
#, kde-format
msgid "Raid"
msgstr "RAID"

#: soldevicetypes.cpp:269
#, kde-format
msgid "Encrypted"
msgstr "Cifrato"

#: soldevicetypes.cpp:272
#, kde-format
msgctxt "unknown volume usage"
msgid "Unknown"
msgstr "Sconosciuto"

#: soldevicetypes.cpp:275
#, kde-format
msgid "File System Type: "
msgstr "Tipo di file system: "

#: soldevicetypes.cpp:275
#, kde-format
msgid "Label: "
msgstr "Etichetta: "

#: soldevicetypes.cpp:276
#, kde-format
msgid "Not Set"
msgstr "Non impostata"

#: soldevicetypes.cpp:276
#, kde-format
msgid "Volume Usage: "
msgstr "Uso del volume: "

#: soldevicetypes.cpp:276
#, kde-format
msgid "UUID: "
msgstr "UUID: "

#: soldevicetypes.cpp:282
#, kde-format
msgid "Mounted At: "
msgstr "Montato su: "

#: soldevicetypes.cpp:282
#, kde-format
msgid "Not Mounted"
msgstr "Non montato"

#: soldevicetypes.cpp:287
#, kde-format
msgid "Volume Space:"
msgstr "Spazio del volume:"

#: soldevicetypes.cpp:297
#, kde-format
msgctxt "Available space out of total partition size (percent used)"
msgid "%1 free of %2 (%3% used)"
msgstr "%1 liberi di %2 (usato %3%)"

#: soldevicetypes.cpp:303
#, kde-format
msgid "No data available"
msgstr "Nessun dato disponibile"

#: soldevicetypes.cpp:330
#, kde-format
msgid "Multimedia Players"
msgstr "Lettori multimediali"

#: soldevicetypes.cpp:349 soldevicetypes.cpp:388
#, kde-format
msgid "Supported Drivers: "
msgstr "Driver supportati:"

#: soldevicetypes.cpp:349 soldevicetypes.cpp:388
#, kde-format
msgid "Supported Protocols: "
msgstr "Protocolli supportati: "

#: soldevicetypes.cpp:369
#, kde-format
msgid "Cameras"
msgstr "Fotocamere"

#: soldevicetypes.cpp:408
#, kde-format
msgid "Batteries"
msgstr "Batterie"

#: soldevicetypes.cpp:430
#, kde-format
msgid "PDA"
msgstr "PDA"

#: soldevicetypes.cpp:433
#, kde-format
msgid "UPS"
msgstr "UPS"

#: soldevicetypes.cpp:436
#, kde-format
msgid "Primary"
msgstr "Primario"

#: soldevicetypes.cpp:439
#, kde-format
msgid "Mouse"
msgstr "Mouse"

#: soldevicetypes.cpp:442
#, kde-format
msgid "Keyboard"
msgstr "Tastiera"

#: soldevicetypes.cpp:445
#, kde-format
msgid "Keyboard + Mouse"
msgstr "Tastiera e mouse"

#: soldevicetypes.cpp:448
#, kde-format
msgid "Camera"
msgstr "Fotocamera"

#: soldevicetypes.cpp:451
#, kde-format
msgid "Phone"
msgstr "Telefono"

#: soldevicetypes.cpp:454
#, kde-format
msgctxt "Screen"
msgid "Monitor"
msgstr "Monitor"

#: soldevicetypes.cpp:457
#, kde-format
msgctxt "Wireless game pad or joystick battery"
msgid "Gaming Input"
msgstr "Input di gioco"

#: soldevicetypes.cpp:460
#, kde-format
msgctxt "unknown battery type"
msgid "Unknown"
msgstr "Sconosciuto"

#: soldevicetypes.cpp:466
#, kde-format
msgid "Charging"
msgstr "In carica"

#: soldevicetypes.cpp:469
#, kde-format
msgid "Discharging"
msgstr "In scarica"

#: soldevicetypes.cpp:472
#, kde-format
msgid "Fully Charged"
msgstr "Batteria carica"

#: soldevicetypes.cpp:475
#, kde-format
msgid "No Charge"
msgstr "Batteria scarica"

#: soldevicetypes.cpp:478
#, kde-format
msgid "Battery Type: "
msgstr "Tipo di batteria: "

#: soldevicetypes.cpp:478
#, kde-format
msgid "Charge Status: "
msgstr "Stato della batteria: "

#: soldevicetypes.cpp:478
#, kde-format
msgid "Charge Percent: "
msgstr "Percentuale di carica: "

#~ msgid "Network Interfaces"
#~ msgstr "Interfacce di rete"

#~ msgid "Connected"
#~ msgstr "Connessa"

#~ msgid "Wireless"
#~ msgstr "Senza fili"

#~ msgid "Wired"
#~ msgstr "Via cavo"

#~ msgid "Hardware Address: "
#~ msgstr "Indirizzo hardware: "

#~ msgid "Wireless?"
#~ msgstr "Senza fili?"

#~ msgid "Audio Interfaces"
#~ msgstr "Interfacce audio"

#~ msgid "Alsa Interfaces"
#~ msgstr "Interfacce ALSA"

#~ msgid "Open Sound System Interfaces"
#~ msgstr "Interfacce OSS"

#~ msgid "Control"
#~ msgstr "Controllo"

#~ msgid "Input"
#~ msgstr "Ingresso"

#~ msgid "Output"
#~ msgstr "Uscita"

#~ msgctxt "unknown audio interface type"
#~ msgid "Unknown"
#~ msgstr "Sconosciuta"

#~ msgid "Internal Soundcard"
#~ msgstr "Scheda sonora interna"

#~ msgid "USB Soundcard"
#~ msgstr "Scheda sonora USB"

#~ msgid "Firewire Soundcard"
#~ msgstr "Scheda sonora FireWire"

#~ msgid "Headset"
#~ msgstr "Cuffie"

#~ msgid "Modem"
#~ msgstr "Modem"

#~ msgctxt "unknown sound card type"
#~ msgid "Unknown"
#~ msgstr "Sconosciuta"

#~ msgid "Audio Interface Type: "
#~ msgstr "Tipo di interfaccia audio: "

#~ msgid "Soundcard Type: "
#~ msgstr "Tipo di scheda sonora: "

#~ msgid "Device Buttons"
#~ msgstr "Pulsanti del dispositivo"

#~ msgid "Lid Button"
#~ msgstr "Pulsante di apertura"

#~ msgid "Power Button"
#~ msgstr "Pulsante di accensione"

#~ msgid "Sleep Button"
#~ msgstr "Pulsante di sospensione"

#~ msgid "Tablet Button"
#~ msgstr "Pulsante di tavoletta grafica"

#~ msgid "Unknown Button"
#~ msgstr "Pulsante sconosciuto"

#~ msgid "Button type: "
#~ msgstr "Tipo di pulsante: "

#~ msgid "Has State?"
#~ msgstr "Ha stato?"

#~ msgid "AC Adapters"
#~ msgstr "Adattatori AC"

#~ msgid "Is plugged in?"
#~ msgstr "Collegato?"

#~ msgid "Digital Video Broadcasting Devices"
#~ msgstr "Dispositivi DVB (TV digitale)"

#~ msgid "Audio"
#~ msgstr "Audio"

#~ msgid "Conditional access system"
#~ msgstr "Sistema ad accesso condizionale"

#~ msgid "Demux"
#~ msgstr "Demux"

#~ msgid "Digital video recorder"
#~ msgstr "Videoregistratore digitale"

#~ msgid "Front end"
#~ msgstr "Interfaccia"

#~ msgid "Network"
#~ msgstr "Rete"

#~ msgid "On-Screen display"
#~ msgstr "Vista a schermo"

#~ msgid "Security and content protection"
#~ msgstr "Sicurezza e protezione del contenuto"

#~ msgid "Video"
#~ msgstr "Video"

#~ msgid "Device Type: "
#~ msgstr "Tipo di dispositivo: "

#~ msgid "Serial Devices"
#~ msgstr "Dispositivi seriali"

#~ msgctxt "platform serial interface type"
#~ msgid "Platform"
#~ msgstr "Piattaforma"

#~ msgctxt "unknown serial interface type"
#~ msgid "Unknown"
#~ msgstr "Sconosciuta"

#~ msgctxt "unknown port"
#~ msgid "Unknown"
#~ msgstr "Sconosciuta"

#~ msgid "Serial Type: "
#~ msgstr "Tipo seriale: "

#~ msgid "Port: "
#~ msgstr "Porta: "

#~ msgid "Smart Card Devices"
#~ msgstr "Dispositivi a smart card"

#~ msgid "Card Reader"
#~ msgstr "Lettore di smart card"

#~ msgid "Crypto Token"
#~ msgstr "Token crittografico"

#~ msgctxt "unknown smart card type"
#~ msgid "Unknown"
#~ msgstr "Sconosciuta"

#~ msgid "Smart Card Type: "
#~ msgstr "Tipo di smart card: "

#~ msgid "Video Devices"
#~ msgstr "Dispositivi video"

#~ msgid "Device unable to be cast to correct device"
#~ msgstr "Impossibile convertire il dispositivo in quello corretto"

#~ msgid "Percentage Used / Available: "
#~ msgstr "Percentuale usata / disponibile: "
