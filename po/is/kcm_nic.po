# translation of kcmnic.po to Icelandic
# Copyright (C) 2003, 2006, 2008, 2010, 2011 Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# Pjetur G. Hjaltason <pjetur@pjetur.net>, 2003.
# Arnar Leósson <leosson@frisurf.no>, 2003.
# Sveinn í Felli <sveinki@nett.is>, 2008, 2010, 2011, 2016.
msgid ""
msgstr ""
"Project-Id-Version: kcmnic\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2022-09-14 00:46+0000\n"
"PO-Revision-Date: 2016-04-08 22:57+0000\n"
"Last-Translator: Sveinn í Felli <sv1@fellsnet.is>\n"
"Language-Team: Icelandic <translation-team-is@lists.sourceforge.net>\n"
"Language: is\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Lokalize 1.5\n"
"Plural-Forms: Plural-Forms: nplurals=2; plural=n != 1;\n"
"\n"
"\n"
"\n"

#, kde-format
msgctxt "NAME OF TRANSLATORS"
msgid "Your names"
msgstr "Sveinn í Felli"

#, kde-format
msgctxt "EMAIL OF TRANSLATORS"
msgid "Your emails"
msgstr "sv1@fellsnet.is"

#: networkmodel.cpp:159
#, fuzzy, kde-format
#| msgid "Point to Point"
msgctxt "@item:intable Network type"
msgid "Point to Point"
msgstr "Punktur til punkts"

#: networkmodel.cpp:166
#, fuzzy, kde-format
#| msgctxt "@item:intext Mode of network card"
#| msgid "Broadcast"
msgctxt "@item:intable Network type"
msgid "Broadcast"
msgstr "Útvörpun"

#: networkmodel.cpp:173
#, fuzzy, kde-format
#| msgid "Multicast"
msgctxt "@item:intable Network type"
msgid "Multicast"
msgstr "Fjölvörpun"

#: networkmodel.cpp:180
#, fuzzy, kde-format
#| msgid "Loopback"
msgctxt "@item:intable Network type"
msgid "Loopback"
msgstr "Hringtenging"

#: nic.cpp:21
#, kde-format
msgid "kcm_nic"
msgstr ""

#: nic.cpp:22
#, fuzzy, kde-format
#| msgid "Network Mask"
msgctxt "@title:window"
msgid "Network Interfaces"
msgstr "Netmöskvi"

#: nic.cpp:26
#, fuzzy, kde-format
#| msgid "(c) 2001 - 2002 Alexander Neundorf"
msgctxt "@info"
msgid "(c) 2001 - 2002 Alexander Neundorf"
msgstr "(c) 2001 - 2002 Alexander Neundorf"

#: nic.cpp:28
#, fuzzy, kde-format
#| msgid "Alexander Neundorf"
msgctxt "@info:credit"
msgid "Alexander Neundorf"
msgstr "Alexander Neundorf"

#: nic.cpp:28
#, kde-format
msgctxt "@info:credit"
msgid "creator"
msgstr ""

#: nic.cpp:29
#, kde-format
msgctxt "@info:credit"
msgid "Carl Schwan"
msgstr ""

#: nic.cpp:29
#, kde-format
msgctxt "@info:credit"
msgid "developer"
msgstr ""

#: package/contents/ui/main.qml:15
#, fuzzy, kde-format
#| msgid "Network Mask"
msgid "Network Interfaces"
msgstr "Netmöskvi"

#: package/contents/ui/main.qml:30
#, fuzzy, kde-format
#| msgid "Name"
msgctxt "@label"
msgid "Name:"
msgstr "Heiti"

#: package/contents/ui/main.qml:34
#, fuzzy, kde-format
#| msgid "IP Address"
msgctxt "@label"
msgid "Address:"
msgstr "IP vistfang"

#: package/contents/ui/main.qml:38
#, fuzzy, kde-format
#| msgid "Network Mask"
msgctxt "@label"
msgid "Network Mask:"
msgstr "Netmöskvi"

#: package/contents/ui/main.qml:42
#, fuzzy, kde-format
#| msgid "Type"
msgctxt "@label"
msgid "Type:"
msgstr "Tegund"

#: package/contents/ui/main.qml:46
#, kde-format
msgctxt "@label"
msgid "Hardware Address:"
msgstr ""

#: package/contents/ui/main.qml:51
#, fuzzy, kde-format
#| msgid "State"
msgctxt "@label"
msgid "State:"
msgstr "Staða"

#: package/contents/ui/main.qml:59
#, kde-format
msgctxt "State of network card is connected"
msgid "Up"
msgstr "Virkt"

#: package/contents/ui/main.qml:60
#, kde-format
msgctxt "State of network card is disconnected"
msgid "Down"
msgstr "Óvirkt"

#: package/contents/ui/main.qml:76
#, kde-format
msgctxt "@action:button"
msgid "Refresh"
msgstr ""

#~ msgctxt "@item:intext Mode of network card"
#~ msgid "Broadcast"
#~ msgstr "Útvörpun"

#~ msgctxt "@item:intext Mode of network card"
#~ msgid "Point to Point"
#~ msgstr "Punktur til punkts"

#~ msgctxt "@item:intext Mode of network card"
#~ msgid "Multicast"
#~ msgstr "Fjölvörpun"

#~ msgctxt "@item:intext Mode of network card"
#~ msgid "Loopback"
#~ msgstr "Hringtenging"

#~ msgctxt "@item:intext Mode of network card"
#~ msgid "Unknown"
#~ msgstr "Óþekkt"

#~ msgctxt "Unknown network mask"
#~ msgid "Unknown"
#~ msgstr "Óþekkt"

#~ msgctxt "Unknown HWaddr"
#~ msgid "Unknown"
#~ msgstr "Óþekkt"

#, fuzzy
#~| msgid "Broadcast"
#~ msgctxt "@item:intable Netork type"
#~ msgid "Broadcast"
#~ msgstr "Útvörpun"

#~ msgid "HWAddr"
#~ msgstr "Vélbúnaðarvistfang"

#~ msgid "&Update"
#~ msgstr "&Uppfæra"

#~ msgid "kcminfo"
#~ msgstr "kcminfo"

#~ msgid "System Information Control Module"
#~ msgstr "Stjórneining kerfisupplýsinga"
