# Burkhard Lück <lueck@hube-lueck.de>, 2015, 2016, 2018, 2019.
# Frederik Schwarzer <schwarzer@kde.org>, 2015, 2020, 2022.
msgid ""
msgstr ""
"Project-Id-Version: \n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2022-10-16 00:48+0000\n"
"PO-Revision-Date: 2022-02-13 20:37+0100\n"
"Last-Translator: Frederik Schwarzer <schwarzer@kde.org>\n"
"Language-Team: German <kde-i18n-doc@kde.org>\n"
"Language: de\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 21.08.0\n"

#, kde-format
msgctxt "NAME OF TRANSLATORS"
msgid "Your names"
msgstr "Burkhard Lück"

#, kde-format
msgctxt "EMAIL OF TRANSLATORS"
msgid "Your emails"
msgstr "lueck@hube-lueck.de"

#: kcm.cpp:41
#, kde-format
msgid "Energy Consumption Statistics"
msgstr "Statistiken über den Energieverbrauch"

#: kcm.cpp:42
#, kde-format
msgid "Kai Uwe Broulik"
msgstr "Kai Uwe Broulik"

#: package/contents/ui/main.qml:22
#, kde-format
msgid "This module lets you see energy information and statistics."
msgstr ""
"In diesem Modul werden Informationen und Statistiken zum Energieverbrauch "
"angezeigt."

#: package/contents/ui/main.qml:49
#, kde-format
msgid "Battery"
msgstr "Akku"

#: package/contents/ui/main.qml:51
#, kde-format
msgid "Rechargeable"
msgstr "Wiederaufladbar"

#: package/contents/ui/main.qml:52
#, kde-format
msgid "Charge state"
msgstr "Ladestatus"

#: package/contents/ui/main.qml:53
#, kde-format
msgid "Current charge"
msgstr "Aktueller Ladezustand"

#: package/contents/ui/main.qml:53 package/contents/ui/main.qml:54
#, kde-format
msgid "%"
msgstr "%"

#: package/contents/ui/main.qml:54
#, kde-format
msgid "Health"
msgstr "Alterungszustand"

#: package/contents/ui/main.qml:55
#, kde-format
msgid "Vendor"
msgstr "Lieferant"

#: package/contents/ui/main.qml:56
#, kde-format
msgid "Model"
msgstr "Modell"

#: package/contents/ui/main.qml:57
#, kde-format
msgid "Serial Number"
msgstr "Seriennummer"

#: package/contents/ui/main.qml:58
#, kde-format
msgid "Technology"
msgstr "Technologie"

#: package/contents/ui/main.qml:62
#, kde-format
msgid "Energy"
msgstr "Energie"

#: package/contents/ui/main.qml:64
#, kde-format
msgctxt "current power draw from the battery in W"
msgid "Consumption"
msgstr "Verbrauch"

#: package/contents/ui/main.qml:64
#, kde-format
msgctxt "Watt"
msgid "W"
msgstr "W"

#: package/contents/ui/main.qml:65
#, kde-format
msgid "Voltage"
msgstr "Spannung"

#: package/contents/ui/main.qml:65
#, kde-format
msgctxt "Volt"
msgid "V"
msgstr "V"

#: package/contents/ui/main.qml:66
#, kde-format
msgid "Remaining energy"
msgstr "Verbleibende Ladung"

#: package/contents/ui/main.qml:66 package/contents/ui/main.qml:67
#: package/contents/ui/main.qml:68
#, kde-format
msgctxt "Watt-hours"
msgid "Wh"
msgstr "Wh"

#: package/contents/ui/main.qml:67
#, kde-format
msgid "Last full charge"
msgstr "Zuletzt vollständig geladen"

#: package/contents/ui/main.qml:68
#, kde-format
msgid "Original charge capacity"
msgstr "Ursprüngliche Ladungskapazität"

#: package/contents/ui/main.qml:72
#, kde-format
msgid "Environment"
msgstr "Umgebung"

#: package/contents/ui/main.qml:74
#, kde-format
msgid "Temperature"
msgstr "Temperatur"

#: package/contents/ui/main.qml:74
#, kde-format
msgctxt "Degree Celsius"
msgid "°C"
msgstr "°C"

#: package/contents/ui/main.qml:81
#, kde-format
msgid "Not charging"
msgstr "Wird nicht geladen"

#: package/contents/ui/main.qml:82
#, kde-format
msgid "Charging"
msgstr "Wird geladen"

#: package/contents/ui/main.qml:83
#, kde-format
msgid "Discharging"
msgstr "Wird entladen"

#: package/contents/ui/main.qml:84
#, kde-format
msgid "Fully charged"
msgstr "Vollständig geladen"

#: package/contents/ui/main.qml:90
#, kde-format
msgid "Lithium ion"
msgstr "Lithium-Ionen"

#: package/contents/ui/main.qml:91
#, kde-format
msgid "Lithium polymer"
msgstr "Lithium-Polymer"

#: package/contents/ui/main.qml:92
#, kde-format
msgid "Lithium iron phosphate"
msgstr "Lithium-Eisen-Phosphat"

#: package/contents/ui/main.qml:93
#, kde-format
msgid "Lead acid"
msgstr "Bleisäure"

#: package/contents/ui/main.qml:94
#, kde-format
msgid "Nickel cadmium"
msgstr "Nickel-Cadmium"

#: package/contents/ui/main.qml:95
#, kde-format
msgid "Nickel metal hydride"
msgstr "Nickel-Metall-Hybrid"

#: package/contents/ui/main.qml:96
#, kde-format
msgid "Unknown technology"
msgstr "Unbekannte Technologie"

#: package/contents/ui/main.qml:103
#, kde-format
msgid "Last hour"
msgstr "Letzte Stunde"

#: package/contents/ui/main.qml:103
#, kde-format
msgid "Last 2 hours"
msgstr "Letzte 2 Stunden"

#: package/contents/ui/main.qml:103
#, kde-format
msgid "Last 12 hours"
msgstr "Letzte 12 Stunden"

#: package/contents/ui/main.qml:103
#, kde-format
msgid "Last 24 hours"
msgstr "Letzte 24 Stunden"

#: package/contents/ui/main.qml:103
#, kde-format
msgid "Last 48 hours"
msgstr "Letzte 48 Stunden"

#: package/contents/ui/main.qml:103
#, kde-format
msgid "Last 7 days"
msgstr "Letzte 7 Tage"

#: package/contents/ui/main.qml:110
#, kde-format
msgctxt "@info:status"
msgid "No energy information available on this system"
msgstr ""
"Auf diesem System sind keine Informationen zum Energieverbrauch verfügbar"

#: package/contents/ui/main.qml:175
#, kde-format
msgid "Internal battery"
msgstr "Interner Akku"

#: package/contents/ui/main.qml:176
#, kde-format
msgid "UPS battery"
msgstr "UPS-Akku"

#: package/contents/ui/main.qml:177
#, kde-format
msgid "Monitor battery"
msgstr "Monitor-Akku"

#: package/contents/ui/main.qml:178
#, kde-format
msgid "Mouse battery"
msgstr "Maus-Akku"

#: package/contents/ui/main.qml:179
#, kde-format
msgid "Keyboard battery"
msgstr "Tastatur-Akku"

#: package/contents/ui/main.qml:180
#, kde-format
msgid "PDA battery"
msgstr "PDA-Akku"

#: package/contents/ui/main.qml:181
#, kde-format
msgid "Phone battery"
msgstr "Telefon-Akku"

#: package/contents/ui/main.qml:182
#, kde-format
msgid "Unknown battery"
msgstr "Unbekannter Akku"

#: package/contents/ui/main.qml:200
#, kde-format
msgctxt "Battery charge percentage"
msgid "%1% (Charging)"
msgstr "%1% (Wird geladen)"

#: package/contents/ui/main.qml:201
#, kde-format
msgctxt "Battery charge percentage"
msgid "%1%"
msgstr "%1 %"

#: package/contents/ui/main.qml:250
#, kde-format
msgctxt "Shorthand for Watts"
msgid "W"
msgstr "W"

#: package/contents/ui/main.qml:250
#, kde-format
msgctxt "literal percent sign"
msgid "%"
msgstr " %"

#: package/contents/ui/main.qml:276
#, kde-format
msgid "Charge Percentage"
msgstr "Ladung in Prozent"

#: package/contents/ui/main.qml:286
#, kde-format
msgid "Energy Consumption"
msgstr "Energieverbrauch"

#: package/contents/ui/main.qml:301
#, kde-format
msgid "Timespan"
msgstr "Zeitraum"

#: package/contents/ui/main.qml:302
#, kde-format
msgid "Timespan of data to display"
msgstr "Zeitraum der anzuzeigenden Daten"

#: package/contents/ui/main.qml:308
#, kde-format
msgid "Refresh"
msgstr "Aktualisieren"

#: package/contents/ui/main.qml:320
#, kde-format
msgid "This type of history is currently not available for this device."
msgstr "Diese Art des Verlaufs ist zurzeit für dieses Gerät nicht verfügbar."

#: package/contents/ui/main.qml:378
#, kde-format
msgid "%1:"
msgstr "%1:"

#: package/contents/ui/main.qml:389
#, kde-format
msgid "Yes"
msgstr "Ja"

#: package/contents/ui/main.qml:391
#, kde-format
msgid "No"
msgstr "Nein"

#: package/contents/ui/main.qml:409
#, kde-format
msgctxt "%1 is value, %2 is unit"
msgid "%1 %2"
msgstr "%1 %2"

#~ msgid "Application Energy Consumption"
#~ msgstr "Energieverbrauch von Programmen"

#~ msgid "Path: %1"
#~ msgstr "Pfad: %1"

#~ msgid "PID: %1"
#~ msgstr "PID: %1"

#~ msgid "Wakeups per second: %1 (%2%)"
#~ msgstr "Weckrufe pro Sekunde: %1 (%2 %)"

#~ msgid "Details: %1"
#~ msgstr "Details: %1"

#~ msgid "Capacity degradation"
#~ msgstr "Kapazitätsverminderung"

#~ msgid "Manufacturer"
#~ msgstr "Hersteller"

#~ msgid "Full design"
#~ msgstr "„Voll“-Design"

#~ msgid "System"
#~ msgstr "System"

#~ msgid "Has power supply"
#~ msgstr "Hat Stromversorgung"

#~ msgid "Capacity"
#~ msgstr "Kapazität"
