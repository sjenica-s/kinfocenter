# Johannes Obermayr <johannesobermayr@gmx.de>, 2010.
# Frederik Schwarzer <schwarzer@kde.org>, 2010, 2016, 2018.
# Burkhard Lück <lueck@hube-lueck.de>, 2010, 2012, 2013, 2014, 2015, 2016, 2020.
msgid ""
msgstr ""
"Project-Id-Version: kcmdevinfo\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2022-10-16 00:48+0000\n"
"PO-Revision-Date: 2020-09-29 08:47+0200\n"
"Last-Translator: Burkhard Lück <lueck@hube-lueck.de>\n"
"Language-Team: German <kde-i18n-de@kde.org>\n"
"Language: de\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"

#, kde-format
msgctxt "NAME OF TRANSLATORS"
msgid "Your names"
msgstr "Johannes Obermayr"

#, kde-format
msgctxt "EMAIL OF TRANSLATORS"
msgid "Your emails"
msgstr "johannesobermayr@gmx.de"

#: devicelisting.cpp:41
#, kde-format
msgctxt "Device Listing Whats This"
msgid "Shows all the devices that are currently listed."
msgstr "Zeigt alle derzeit aufgelisteten Geräte an."

#: devicelisting.cpp:44
#, kde-format
msgid "Devices"
msgstr "Geräte"

#: devicelisting.cpp:57
#, kde-format
msgid "Collapse All"
msgstr "Alle einklappen"

#: devicelisting.cpp:60
#, kde-format
msgid "Expand All"
msgstr "Alle aufklappen"

#: devicelisting.cpp:63
#, kde-format
msgid "Show All Devices"
msgstr "Alle Geräte anzeigen"

#: devicelisting.cpp:66
#, kde-format
msgid "Show Relevant Devices"
msgstr "Wichtige Geräte anzeigen"

#: devicelisting.cpp:95
#, kde-format
msgctxt "unknown device type"
msgid "Unknown"
msgstr "Unbekannt"

#: devicelisting.cpp:136 devinfo.cpp:79
#, kde-format
msgctxt "no device UDI"
msgid "None"
msgstr "Keine"

#: devinfo.cpp:28
#, kde-format
msgid "kcmdevinfo"
msgstr "kcmdevinfo"

#: devinfo.cpp:28
#, kde-format
msgid "Device Viewer"
msgstr "Gerätebetrachter"

#: devinfo.cpp:28
#, kde-format
msgid "(c) 2010 David Hubner"
msgstr "© 2010, David Hubner"

#: devinfo.cpp:58
#, kde-format
msgid "UDI: "
msgstr "UDI: "

#: devinfo.cpp:66
#, kde-format
msgctxt "Udi Whats This"
msgid "Shows the current device's UDI (Unique Device Identifier)"
msgstr "Zeigt die UDI (eindeutige Gerätebezeichnung) des aktuellen Geräts an."

#: infopanel.cpp:20
#, kde-format
msgid "Device Information"
msgstr "Geräteinformationen"

#: infopanel.cpp:29
#, kde-format
msgctxt "Info Panel Whats This"
msgid "Shows information about the currently selected device."
msgstr "Zeigt Informationen zum aktuell ausgewählten Gerät an."

#: infopanel.cpp:56
#, kde-format
msgid ""
"\n"
"Solid Based Device Viewer Module"
msgstr ""
"\n"
"Solid-basiertes Gerätebetrachtungsmodul"

#: infopanel.cpp:119
#, kde-format
msgid "Description: "
msgstr "Beschreibung: "

#: infopanel.cpp:121
#, kde-format
msgid "Product: "
msgstr "Produkt: "

#: infopanel.cpp:123
#, kde-format
msgid "Vendor: "
msgstr "Hersteller: "

#: infopanel.cpp:145
#, kde-format
msgid "Yes"
msgstr "Ja"

#: infopanel.cpp:147
#, kde-format
msgid "No"
msgstr "Nein"

#: infopanel.h:36
#, kde-format
msgctxt "name of something is not known"
msgid "Unknown"
msgstr "Unbekannt"

#: soldevice.cpp:65
#, kde-format
msgctxt "unknown device"
msgid "Unknown"
msgstr "Unbekannt"

#: soldevice.cpp:91
#, kde-format
msgctxt "Default device tooltip"
msgid "A Device"
msgstr "Ein Gerät"

#: soldevicetypes.cpp:43
#, kde-format
msgid "Processors"
msgstr "Prozessoren"

#: soldevicetypes.cpp:59
#, kde-format
msgid "Processor %1"
msgstr "Prozessor %1"

#: soldevicetypes.cpp:76
#, kde-format
msgid "Intel MMX"
msgstr "Intel MMX"

#: soldevicetypes.cpp:79
#, kde-format
msgid "Intel SSE"
msgstr "Intel SSE"

#: soldevicetypes.cpp:82
#, kde-format
msgid "Intel SSE2"
msgstr "Intel SSE2"

#: soldevicetypes.cpp:85
#, kde-format
msgid "Intel SSE3"
msgstr "Intel SSE3"

#: soldevicetypes.cpp:88
#, kde-format
msgid "Intel SSSE3"
msgstr "Intel SSSE3"

#: soldevicetypes.cpp:91
#, kde-format
msgid "Intel SSE4.1"
msgstr "Intel SSE4.1"

#: soldevicetypes.cpp:94
#, kde-format
msgid "Intel SSE4.2"
msgstr "Intel SSE4.2"

#: soldevicetypes.cpp:97
#, kde-format
msgid "AMD 3DNow!"
msgstr "AMD 3DNow!"

#: soldevicetypes.cpp:100
#, kde-format
msgid "ATI IVEC"
msgstr "ATI IVEC"

#: soldevicetypes.cpp:103
#, kde-format
msgctxt "no instruction set extensions"
msgid "None"
msgstr "Keine"

#: soldevicetypes.cpp:106
#, kde-format
msgid "Processor Number: "
msgstr "Prozessornummer: "

#: soldevicetypes.cpp:106
#, kde-format
msgid "Max Speed: "
msgstr "Maximalgeschwindigkeit: "

#: soldevicetypes.cpp:107
#, kde-format
msgid "Supported Instruction Sets: "
msgstr "Unterstützte Instruktions-Sätze: "

#: soldevicetypes.cpp:132
#, kde-format
msgid "Storage Drives"
msgstr "Speichergeräte"

#: soldevicetypes.cpp:151
#, kde-format
msgid "Hard Disk Drive"
msgstr "Festplatte"

#: soldevicetypes.cpp:154
#, kde-format
msgid "Compact Flash Reader"
msgstr "Lesegerät (Compact Flash)"

#: soldevicetypes.cpp:157
#, kde-format
msgid "Smart Media Reader"
msgstr "Lesegerät (Smart Media)"

#: soldevicetypes.cpp:160
#, kde-format
msgid "SD/MMC Reader"
msgstr "Lesegerät (SD/MMC)"

#: soldevicetypes.cpp:163
#, kde-format
msgid "Optical Drive"
msgstr "Optisches Laufwerk"

#: soldevicetypes.cpp:166
#, kde-format
msgid "Memory Stick Reader"
msgstr "Lesegerät (Memory Stick)"

#: soldevicetypes.cpp:169
#, kde-format
msgid "xD Reader"
msgstr "Lesegerät (xD)"

#: soldevicetypes.cpp:172
#, kde-format
msgid "Unknown Drive"
msgstr "Unbekanntes Laufwerk"

#: soldevicetypes.cpp:192
#, kde-format
msgid "IDE"
msgstr "IDE"

#: soldevicetypes.cpp:195
#, kde-format
msgid "USB"
msgstr "USB"

#: soldevicetypes.cpp:198
#, kde-format
msgid "IEEE1394"
msgstr "IEEE 1394"

#: soldevicetypes.cpp:201
#, kde-format
msgid "SCSI"
msgstr "SCSI"

#: soldevicetypes.cpp:204
#, kde-format
msgid "SATA"
msgstr "SATA"

#: soldevicetypes.cpp:207
#, kde-format
msgctxt "platform storage bus"
msgid "Platform"
msgstr "Plattform"

#: soldevicetypes.cpp:210
#, kde-format
msgctxt "unknown storage bus"
msgid "Unknown"
msgstr "Unbekannt"

#: soldevicetypes.cpp:213
#, kde-format
msgid "Bus: "
msgstr "Bus: "

#: soldevicetypes.cpp:213
#, kde-format
msgid "Hotpluggable?"
msgstr "Hotplug-fähig?"

#: soldevicetypes.cpp:213
#, kde-format
msgid "Removable?"
msgstr "Wechselbar?"

#: soldevicetypes.cpp:257
#, kde-format
msgid "Unused"
msgstr "Nicht verwendet"

#: soldevicetypes.cpp:260
#, kde-format
msgid "File System"
msgstr "Dateisystem"

#: soldevicetypes.cpp:263
#, kde-format
msgid "Partition Table"
msgstr "Partitionstabelle"

#: soldevicetypes.cpp:266
#, kde-format
msgid "Raid"
msgstr "RAID"

#: soldevicetypes.cpp:269
#, kde-format
msgid "Encrypted"
msgstr "Verschlüsselt"

#: soldevicetypes.cpp:272
#, kde-format
msgctxt "unknown volume usage"
msgid "Unknown"
msgstr "Unbekannt"

#: soldevicetypes.cpp:275
#, kde-format
msgid "File System Type: "
msgstr "Dateisystemtyp: "

#: soldevicetypes.cpp:275
#, kde-format
msgid "Label: "
msgstr "Bezeichnung: "

#: soldevicetypes.cpp:276
#, kde-format
msgid "Not Set"
msgstr "Nicht festgelegt"

#: soldevicetypes.cpp:276
#, kde-format
msgid "Volume Usage: "
msgstr "Speicherplatzverwendung: "

#: soldevicetypes.cpp:276
#, kde-format
msgid "UUID: "
msgstr "UUID: "

#: soldevicetypes.cpp:282
#, kde-format
msgid "Mounted At: "
msgstr "Eingebunden bei: "

#: soldevicetypes.cpp:282
#, kde-format
msgid "Not Mounted"
msgstr "Nicht eingebunden"

#: soldevicetypes.cpp:287
#, kde-format
msgid "Volume Space:"
msgstr "Datenträgergröße: "

#: soldevicetypes.cpp:297
#, kde-format
msgctxt "Available space out of total partition size (percent used)"
msgid "%1 free of %2 (%3% used)"
msgstr "%1 von %2 frei (%3 % belegt)"

#: soldevicetypes.cpp:303
#, kde-format
msgid "No data available"
msgstr "Keine Daten verfügbar"

#: soldevicetypes.cpp:330
#, kde-format
msgid "Multimedia Players"
msgstr "Medienspieler"

#: soldevicetypes.cpp:349 soldevicetypes.cpp:388
#, kde-format
msgid "Supported Drivers: "
msgstr "Unterstützte Treiber: "

#: soldevicetypes.cpp:349 soldevicetypes.cpp:388
#, kde-format
msgid "Supported Protocols: "
msgstr "Unterstützte Protokolle: "

#: soldevicetypes.cpp:369
#, kde-format
msgid "Cameras"
msgstr "Kameras"

#: soldevicetypes.cpp:408
#, kde-format
msgid "Batteries"
msgstr "Akkus"

#: soldevicetypes.cpp:430
#, kde-format
msgid "PDA"
msgstr "PDA"

#: soldevicetypes.cpp:433
#, kde-format
msgid "UPS"
msgstr "USV"

#: soldevicetypes.cpp:436
#, kde-format
msgid "Primary"
msgstr "Primär"

#: soldevicetypes.cpp:439
#, kde-format
msgid "Mouse"
msgstr "Maus"

#: soldevicetypes.cpp:442
#, kde-format
msgid "Keyboard"
msgstr "Tastatur"

#: soldevicetypes.cpp:445
#, kde-format
msgid "Keyboard + Mouse"
msgstr "Tastatur && Maus"

#: soldevicetypes.cpp:448
#, kde-format
msgid "Camera"
msgstr "Kamera"

#: soldevicetypes.cpp:451
#, kde-format
msgid "Phone"
msgstr "Telefon"

#: soldevicetypes.cpp:454
#, kde-format
msgctxt "Screen"
msgid "Monitor"
msgstr "Monitor"

#: soldevicetypes.cpp:457
#, kde-format
msgctxt "Wireless game pad or joystick battery"
msgid "Gaming Input"
msgstr "Spiel-Eingabegerät"

#: soldevicetypes.cpp:460
#, kde-format
msgctxt "unknown battery type"
msgid "Unknown"
msgstr "Unbekannt"

#: soldevicetypes.cpp:466
#, kde-format
msgid "Charging"
msgstr "Wird geladen"

#: soldevicetypes.cpp:469
#, kde-format
msgid "Discharging"
msgstr "Wird entladen"

#: soldevicetypes.cpp:472
#, kde-format
msgid "Fully Charged"
msgstr "Vollständig geladen"

#: soldevicetypes.cpp:475
#, kde-format
msgid "No Charge"
msgstr "Nicht geladen"

#: soldevicetypes.cpp:478
#, kde-format
msgid "Battery Type: "
msgstr "Akku-Typ: "

#: soldevicetypes.cpp:478
#, kde-format
msgid "Charge Status: "
msgstr "Ladestatus: "

#: soldevicetypes.cpp:478
#, kde-format
msgid "Charge Percent: "
msgstr "Ladestand:"

#~ msgid "Network Interfaces"
#~ msgstr "Netzwerkschnittstellen"

#~ msgid "Connected"
#~ msgstr "Verbunden"

#~ msgid "Wireless"
#~ msgstr "Drahtlos"

#~ msgid "Wired"
#~ msgstr "Kabelgebunden"

#~ msgid "Hardware Address: "
#~ msgstr "Hardware-Adresse: "

#~ msgid "Wireless?"
#~ msgstr "Drahtlos?"

#~ msgid "Audio Interfaces"
#~ msgstr "Audiogeräte"

#~ msgid "Alsa Interfaces"
#~ msgstr "Alsa-Geräte"

#~ msgid "Open Sound System Interfaces"
#~ msgstr "OSS-Geräte (Open Sound System)"

#~ msgid "Control"
#~ msgstr "Steuerung"

#~ msgid "Input"
#~ msgstr "Eingabe"

#~ msgid "Output"
#~ msgstr "Ausgabe"

#~ msgctxt "unknown audio interface type"
#~ msgid "Unknown"
#~ msgstr "Unbekannt"

#~ msgid "Internal Soundcard"
#~ msgstr "Interne Soundkarte"

#~ msgid "USB Soundcard"
#~ msgstr "USB-Soundkarte"

#~ msgid "Firewire Soundcard"
#~ msgstr "Firewire-Soundkarte"

#~ msgid "Headset"
#~ msgstr "Headset"

#~ msgid "Modem"
#~ msgstr "Modem"

#~ msgctxt "unknown sound card type"
#~ msgid "Unknown"
#~ msgstr "Unbekannt"

#~ msgid "Audio Interface Type: "
#~ msgstr "Audiogeräte-Typ: "

#~ msgid "Soundcard Type: "
#~ msgstr "Soundkartentyp: "

#~ msgid "Device Buttons"
#~ msgstr "Geräteknöpfe"

#~ msgid "Lid Button"
#~ msgstr "Bildschirm-Schalter"

#~ msgid "Power Button"
#~ msgstr "Netzschalter"

#~ msgid "Sleep Button"
#~ msgstr "Ruhezustand-Knopf"

#~ msgid "Tablet Button"
#~ msgstr "Tablet-Knopf"

#~ msgid "Unknown Button"
#~ msgstr "Unbekannter Knopf"

#~ msgid "Button type: "
#~ msgstr "Knopftyp: "

#~ msgid "Has State?"
#~ msgstr "Status?"

#~ msgid "AC Adapters"
#~ msgstr "Netzteile"

#~ msgid "Is plugged in?"
#~ msgstr "Angeschlossen?"

#~ msgid "Digital Video Broadcasting Devices"
#~ msgstr "DVB-Geräte"

#~ msgid "Audio"
#~ msgstr "Audio"

#~ msgid "Conditional access system"
#~ msgstr "Abhängiges Zugriffssystem"

#~ msgid "Demux"
#~ msgstr "Demux"

#~ msgid "Digital video recorder"
#~ msgstr "Digital-Video-Recorder (DVR)"

#~ msgid "Front end"
#~ msgstr "Oberfläche"

#~ msgid "Network"
#~ msgstr "Netzwerk"

#~ msgid "On-Screen display"
#~ msgstr "On-Screen-Display (OSD)"

#~ msgid "Security and content protection"
#~ msgstr "Sicherheit und Inhaltsschutz"

#~ msgid "Video"
#~ msgstr "Video"

#~ msgid "Device Type: "
#~ msgstr "Gerätetyp: "

#~ msgid "Serial Devices"
#~ msgstr "Serielle Geräte"

#~ msgctxt "platform serial interface type"
#~ msgid "Platform"
#~ msgstr "Plattform"

#~ msgctxt "unknown serial interface type"
#~ msgid "Unknown"
#~ msgstr "Unbekannt"

#~ msgctxt "unknown port"
#~ msgid "Unknown"
#~ msgstr "Unbekannt"

#~ msgid "Serial Type: "
#~ msgstr "Serieller Typ: "

#~ msgid "Port: "
#~ msgstr "Port: "

#~ msgid "Smart Card Devices"
#~ msgstr "Smartcard-Geräte"

#~ msgid "Card Reader"
#~ msgstr "Kartenleser"

#~ msgid "Crypto Token"
#~ msgstr "Chipschlüssel"

#~ msgctxt "unknown smart card type"
#~ msgid "Unknown"
#~ msgstr "Unbekannt"

#~ msgid "Smart Card Type: "
#~ msgstr "Smartcard-Typ: "

#~ msgid "Video Devices"
#~ msgstr "Videogeräte"

#~ msgid "Device unable to be cast to correct device"
#~ msgstr "Das Gerät kann nicht dem richtigen Gerät zugeordnet werden."

#~ msgid "Percentage Used / Available: "
#~ msgstr "Verwendet / Verfügbar (prozentual): "

#~ msgid "Not Connected"
#~ msgstr "Nicht verbunden"

#~ msgid "IP Address (V4): "
#~ msgstr "IP-Adresse (v4): "

#~ msgid "Gateway (V4): "
#~ msgstr "Gateway (v4): "

#~ msgid "Sd Mmc"
#~ msgstr "SD/MMC"

#~ msgid "Xd"
#~ msgstr "Xd"

#~ msgid " Drive"
#~ msgstr " Laufwerk"
