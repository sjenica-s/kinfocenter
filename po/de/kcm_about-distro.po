# Burkhard Lück <lueck@hube-lueck.de>, 2013, 2014, 2015, 2016, 2018, 2019, 2020, 2021.
# Frederik Schwarzer <schwarzer@kde.org>, 2014, 2015, 2016, 2022.
msgid ""
msgstr ""
"Project-Id-Version: \n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2022-11-04 00:47+0000\n"
"PO-Revision-Date: 2022-07-23 22:13+0200\n"
"Last-Translator: Frederik Schwarzer <schwarzer@kde.org>\n"
"Language-Team: German <kde-i18n-doc@kde.org>\n"
"Language: de\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 22.07.70\n"

#, kde-format
msgctxt "NAME OF TRANSLATORS"
msgid "Your names"
msgstr "Burkhard Lück"

#, kde-format
msgctxt "EMAIL OF TRANSLATORS"
msgid "Your emails"
msgstr "lueck@hube-lueck.de"

#: CPUEntry.cpp:17
#, kde-format
msgid "Processor:"
msgid_plural "Processors:"
msgstr[0] "Prozessor:"
msgstr[1] "Prozessoren:"

#: CPUEntry.cpp:22
#, kde-format
msgctxt ""
"unknown CPU type/product name; presented as `Processors: 4 × Unknown Type'"
msgid "Unknown Type"
msgstr "Unbekannter Typ"

#: GPUEntry.cpp:19
#, kde-format
msgid "Graphics Processor:"
msgstr "Grafikprozessor:"

#: GraphicsPlatformEntry.cpp:10
#, kde-format
msgid "Graphics Platform:"
msgstr "Grafik-Plattform:"

#: KernelEntry.cpp:11
#, kde-format
msgid "Kernel Version:"
msgstr "Kernel-Version:"

#: KernelEntry.cpp:23
#, kde-format
msgctxt "@label %1 is the kernel version, %2 CPU bit width (e.g. 32 or 64)"
msgid "%1 (%2-bit)"
msgstr "%1 (%2-bit)"

#: main.cpp:104
#, kde-format
msgctxt "@title"
msgid "About this System"
msgstr "Über dieses System"

#: main.cpp:107
#, kde-format
msgctxt "@info:credit"
msgid "Copyright 2012-2020 Harald Sitter"
msgstr "Copyright © 2012-2020 Harald Sitter"

#: main.cpp:108
#, kde-format
msgctxt "@info:credit"
msgid "Harald Sitter"
msgstr "Harald Sitter"

#: main.cpp:108
#, kde-format
msgctxt "@info:credit"
msgid "Author"
msgstr "Autor"

#: main.cpp:177
#, kde-format
msgctxt "@label"
msgid "Manufacturer:"
msgstr "Hersteller:"

#: main.cpp:180 main.cpp:197
#, kde-format
msgctxt "@label"
msgid "Product Name:"
msgstr "Produktname:"

#: main.cpp:183
#, kde-format
msgctxt "@label"
msgid "System Version:"
msgstr "Systemversion:"

#: main.cpp:186 main.cpp:200
#, kde-format
msgctxt "@label"
msgid "Serial Number:"
msgstr "Seriennummer:"

#: main.cpp:190 main.cpp:207
#, kde-format
msgctxt "@label unknown entry in table"
msgid "Unknown:"
msgstr "Unbekannt:"

#: main.cpp:203
#, kde-format
msgctxt "@label uboot is the name of a bootloader for embedded devices"
msgid "U-Boot Version:"
msgstr "U-Boot-Version:"

#: main.cpp:241
#, kde-format
msgid "KDE Frameworks Version:"
msgstr "KDE-Frameworks-Version:"

#: main.cpp:242
#, kde-format
msgid "Qt Version:"
msgstr "Qt-Version:"

#: MemoryEntry.cpp:20
#, kde-format
msgid "Memory:"
msgstr "Speicher:"

#: MemoryEntry.cpp:49
#, kde-format
msgctxt "@label %1 is the formatted amount of system memory (e.g. 7,7 GiB)"
msgid "%1 of RAM"
msgstr "%1 Arbeitsspeicher"

#: MemoryEntry.cpp:53
#, kde-format
msgctxt "Unknown amount of RAM"
msgid "Unknown"
msgstr "Unbekannt"

#: OSVersionEntry.cpp:9
#, kde-format
msgid "Operating System:"
msgstr "Betriebssystem:"

#: OSVersionEntry.cpp:11
#, fuzzy, kde-format
#| msgctxt ""
#| "%1 is a label already including a colon, %2 is the corresponding value"
#| msgid "%1 %2"
msgctxt "@label %1 is the distro name, %2 is the version"
msgid "%1 %2"
msgstr "%1 %2"

#: OSVersionEntry.cpp:13
#, kde-format
msgctxt ""
"@label %1 is the distro name, %2 is the version, %3 is the 'build' which "
"should be a number, or 'rolling'"
msgid "%1 %2 Build: %3"
msgstr ""

#: package/contents/ui/main.qml:91
#, kde-format
msgctxt "@title"
msgid "Serial Number"
msgstr "Seriennummer"

#: package/contents/ui/main.qml:97 package/contents/ui/main.qml:174
#, kde-format
msgctxt "@action:button"
msgid "Copy to Clipboard"
msgstr "In die Zwischenablage kopieren"

#: package/contents/ui/main.qml:122
#, kde-format
msgctxt "@action:button show a hidden entry in an overlay"
msgid "Show"
msgstr "Anzeigen"

#: package/contents/ui/main.qml:132
#, kde-format
msgctxt "@title:group"
msgid "Software"
msgstr "Software"

#: package/contents/ui/main.qml:142
#, kde-format
msgctxt "@title:group"
msgid "Hardware"
msgstr "Hardware"

#: package/contents/ui/main.qml:162
#, kde-format
msgctxt "@action:button launches kinfocenter from systemsettings"
msgid "Show More Information"
msgstr "Weitere Informationen anzeigen"

#: package/contents/ui/main.qml:181
#, kde-format
msgctxt "@action:button"
msgid "Copy to Clipboard in English"
msgstr "In die Zwischenablage auf Englisch kopieren"

#: PlasmaEntry.cpp:15
#, kde-format
msgid "KDE Plasma Version:"
msgstr "KDE-Plasma-Version:"

#~ msgid "Copy software and hardware information to clipboard"
#~ msgstr ""
#~ "Informationen über Software und Hardware in die Zwischenablage kopieren"

#~ msgid "Copy software and hardware information to clipboard in English"
#~ msgstr ""
#~ "Informationen über Software und Hardware in englischer Sprache in die "
#~ "Zwischenablage kopieren"

#~ msgid "OS Type:"
#~ msgstr "Art des Betriebssystems:"

#~ msgctxt "@title"
#~ msgid "About Distribution"
#~ msgstr "Über Distribution"

#~ msgid "{variantLabel}"
#~ msgstr "{variantLabel}"

#~ msgctxt "@label %1 is the CPU bit width (e.g. 32 or 64)"
#~ msgid "<numid>%1</numid>-bit"
#~ msgstr "<numid>%1</numid>-bit"

#~ msgctxt "@label Unknown distribution name (e.g. MyDistro)"
#~ msgid "Unknown"
#~ msgstr "Unbekannt"

#~ msgctxt "@label Unknown distribution version (e.g. 1.0)"
#~ msgid "Unknown"
#~ msgstr "Unbekannt"

#~ msgctxt ""
#~ "@label Unknown distribution codename (e.g. for Kubuntu this would be "
#~ "'raring' or 'saucy')"
#~ msgid "Unknown"
#~ msgstr "Unbekannt"

#~ msgctxt ""
#~ "@label Unknown distribution description (this often is a combination of "
#~ "name and version, such as 'MyDistro 1.0'"
#~ msgid "Unknown"
#~ msgstr "Unbekannt"

#~ msgid "DESCRIPTION..."
#~ msgstr "Beschreibung ..."
