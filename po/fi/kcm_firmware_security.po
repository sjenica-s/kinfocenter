# Copyright (C) YEAR This file is copyright:
# This file is distributed under the same license as the kinfocenter package.
# Tommi Nieminen <translator@legisign.org>, 2022.
#
msgid ""
msgstr ""
"Project-Id-Version: kinfocenter\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2022-03-05 00:45+0000\n"
"PO-Revision-Date: 2022-03-18 23:26+0200\n"
"Last-Translator: Tommi Nieminen <translator@legisign.org>\n"
"Language-Team: Finnish <kde-i18n-doc@kde.org>\n"
"Language: fi\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Lokalize 20.04.2\n"

#, kde-format
msgctxt "NAME OF TRANSLATORS"
msgid "Your names"
msgstr "Tommi Nieminen"

#, kde-format
msgctxt "EMAIL OF TRANSLATORS"
msgid "Your emails"
msgstr "translator@legisign.org"

#: main.cpp:22
#, kde-format
msgctxt "@label kcm name"
msgid "Firmware Security"
msgstr "Laiteohjelmiston turvallisuus"

#: main.cpp:26
#, kde-format
msgid "Harald Sitter"
msgstr "Harald Sitter"

#: package/contents/ui/main.qml:14
msgctxt "@info:whatsthis"
msgid "Firmware Security Information"
msgstr "Laiteohjelmiston turvallisuustiedot"
