# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
#
# Kristóf Kiszel <ulysses@kubuntu.org>, 2010, 2021.
msgid ""
msgstr ""
"Project-Id-Version: \n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2022-09-14 00:46+0000\n"
"PO-Revision-Date: 2021-01-03 14:02+0100\n"
"Last-Translator: Kristóf Kiszel <kiszel.kristof@gmail.com>\n"
"Language-Team: Hungarian <kde-l10n-hu@kde.org>\n"
"Language: hu\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Lokalize 21.03.70\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#, kde-format
msgctxt "NAME OF TRANSLATORS"
msgid "Your names"
msgstr "Kiszel Kristóf"

#, kde-format
msgctxt "EMAIL OF TRANSLATORS"
msgid "Your emails"
msgstr "kiszel.kristof@gmail.com"

#: networkmodel.cpp:159
#, kde-format
msgctxt "@item:intable Network type"
msgid "Point to Point"
msgstr "Pont-pont"

#: networkmodel.cpp:166
#, kde-format
msgctxt "@item:intable Network type"
msgid "Broadcast"
msgstr "Broadcast"

#: networkmodel.cpp:173
#, kde-format
msgctxt "@item:intable Network type"
msgid "Multicast"
msgstr "Multicast"

#: networkmodel.cpp:180
#, kde-format
msgctxt "@item:intable Network type"
msgid "Loopback"
msgstr "Visszacsatolás"

#: nic.cpp:21
#, kde-format
msgid "kcm_nic"
msgstr "kcm_nic"

#: nic.cpp:22
#, kde-format
msgctxt "@title:window"
msgid "Network Interfaces"
msgstr "Hálózati interfészek"

#: nic.cpp:26
#, kde-format
msgctxt "@info"
msgid "(c) 2001 - 2002 Alexander Neundorf"
msgstr "(C) Alexander Neundorf, 2001-2002."

#: nic.cpp:28
#, kde-format
msgctxt "@info:credit"
msgid "Alexander Neundorf"
msgstr "Alexander Neundorf"

#: nic.cpp:28
#, kde-format
msgctxt "@info:credit"
msgid "creator"
msgstr "Készítő"

#: nic.cpp:29
#, kde-format
msgctxt "@info:credit"
msgid "Carl Schwan"
msgstr "Carl Schwan"

#: nic.cpp:29
#, kde-format
msgctxt "@info:credit"
msgid "developer"
msgstr "Fejlesztő"

#: package/contents/ui/main.qml:15
#, kde-format
msgid "Network Interfaces"
msgstr "Hálózati interfészek"

#: package/contents/ui/main.qml:30
#, kde-format
msgctxt "@label"
msgid "Name:"
msgstr "Név:"

#: package/contents/ui/main.qml:34
#, kde-format
msgctxt "@label"
msgid "Address:"
msgstr "Cím:"

#: package/contents/ui/main.qml:38
#, kde-format
msgctxt "@label"
msgid "Network Mask:"
msgstr "Hálózati maszk:"

#: package/contents/ui/main.qml:42
#, kde-format
msgctxt "@label"
msgid "Type:"
msgstr "Típus:"

#: package/contents/ui/main.qml:46
#, kde-format
msgctxt "@label"
msgid "Hardware Address:"
msgstr "Hardvercím:"

#: package/contents/ui/main.qml:51
#, kde-format
msgctxt "@label"
msgid "State:"
msgstr "Állapot:"

#: package/contents/ui/main.qml:59
#, kde-format
msgctxt "State of network card is connected"
msgid "Up"
msgstr "Fel"

#: package/contents/ui/main.qml:60
#, kde-format
msgctxt "State of network card is disconnected"
msgid "Down"
msgstr "Le"

#: package/contents/ui/main.qml:76
#, kde-format
msgctxt "@action:button"
msgid "Refresh"
msgstr "Frissítés"

#~ msgctxt "@item:intext Mode of network card"
#~ msgid "Broadcast"
#~ msgstr "Broadcast"

#~ msgctxt "@item:intext Mode of network card"
#~ msgid "Point to Point"
#~ msgstr "Pont-pont"

#~ msgctxt "@item:intext Mode of network card"
#~ msgid "Multicast"
#~ msgstr "Multicast"

#~ msgctxt "@item:intext Mode of network card"
#~ msgid "Loopback"
#~ msgstr "Visszacsatolás"

#~ msgctxt "@item:intext Mode of network card"
#~ msgid "Unknown"
#~ msgstr "Ismeretlen"

#~ msgctxt "Unknown network mask"
#~ msgid "Unknown"
#~ msgstr "Ismeretlen"

#~ msgctxt "Unknown HWaddr"
#~ msgid "Unknown"
#~ msgstr "Ismeretlen"

#, fuzzy
#~| msgid "Broadcast"
#~ msgctxt "@item:intable Netork type"
#~ msgid "Broadcast"
#~ msgstr "Broadcast"

#~ msgid "HWAddr"
#~ msgstr "HWAddr"

#~ msgid "&Update"
#~ msgstr "&Frissítés"

#~ msgid "kcminfo"
#~ msgstr "kcminfo"

#~ msgid "System Information Control Module"
#~ msgstr "Rendszerinformációs vezérlőmodul"
