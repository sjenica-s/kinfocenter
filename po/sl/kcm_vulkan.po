# Copyright (C) YEAR This file is copyright:
# This file is distributed under the same license as the kinfocenter package.
#
# Matjaž Jeran <matjaz.jeran@amis.net>, 2021.
msgid ""
msgstr ""
"Project-Id-Version: kinfocenter\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2021-11-17 00:29+0000\n"
"PO-Revision-Date: 2021-08-12 07:43+0200\n"
"Last-Translator: Matjaž Jeran <matjaz.jeran@amis.net>\n"
"Language-Team: Slovenian <lugos-slo@lugos.si>\n"
"Language: sl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Lokalize 20.12.2\n"
"Plural-Forms: nplurals=4; plural=(n%100==1 ? 1 : n%100==2 ? 2 : n%100==3 || n"
"%100==4 ? 3 : 0);\n"

#, kde-format
msgctxt "NAME OF TRANSLATORS"
msgid "Your names"
msgstr "Matjaž Jeran"

#, kde-format
msgctxt "EMAIL OF TRANSLATORS"
msgid "Your emails"
msgstr "matjaz.jeran@amis.net"

#: main.cpp:23
#, kde-format
msgctxt "@label kcm name"
msgid "Vulkan"
msgstr "Vulkan"

#: main.cpp:24
#, kde-format
msgid "Harald Sitter"
msgstr "Harald Sitter"

#: package/contents/ui/main.qml:14
msgctxt "@info"
msgid "Vulkan Graphics API Information"
msgstr "Informacija o vmesniku Vulkan Graphics"

#~ msgctxt "@info"
#~ msgid "The executable <command>%1</command> could not be found in $PATH."
#~ msgstr ""
#~ "Izvajalnega programa <command>%1</command>ni bilo mogoče najti na $PATH."

#~ msgctxt "@info"
#~ msgid ""
#~ "The subprocess <command>%1</command> crashed unexpectedly. No data could "
#~ "be obtained."
#~ msgstr ""
#~ "Podprocess <command>%1</command> se je nepričakovano sesul. Nobenega "
#~ "podatka o tem ni bilo mogoče dobiti."

#~ msgctxt "@label placeholder text to filter for something"
#~ msgid "Filter…"
#~ msgstr "Filter…"

#~ msgctxt "accessible name for filter input"
#~ msgid "Filter"
#~ msgstr "Filter"
