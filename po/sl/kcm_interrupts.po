# Copyright (C) YEAR This file is copyright:
# This file is distributed under the same license as the kinfocenter package.
#
# Matjaž Jeran <matjaz.jeran@amis.net>, 2021.
msgid ""
msgstr ""
"Project-Id-Version: kinfocenter\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2022-07-05 00:46+0000\n"
"PO-Revision-Date: 2022-01-07 10:24+0100\n"
"Last-Translator: Matjaž Jeran <matjaz.jeran@amis.net>\n"
"Language-Team: Slovenian <lugos-slo@lugos.si>\n"
"Language: sl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=(n%100==1 ? 1 : n%100==2 ? 2 : n%100==3 || n"
"%100==4 ? 3 : 0);\n"
"X-Generator: Poedit 3.0.1\n"

#, kde-format
msgctxt "NAME OF TRANSLATORS"
msgid "Your names"
msgstr "Matjaž Jeran"

#, kde-format
msgctxt "EMAIL OF TRANSLATORS"
msgid "Your emails"
msgstr "matjaz.jeran@amis.net"

#: main.cpp:28
#, kde-format
msgctxt "@label kcm name"
msgid "Interrupts"
msgstr "Prekinitve"

#: main.cpp:29
#, kde-format
msgid "Harald Sitter"
msgstr "Harald Sitter"

#: package/contents/ui/main.qml:14
msgctxt "@info"
msgid "CPU Interrupts Information"
msgstr "Informacije o prekinitvah CPE"
