# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
#
# g.sora <g.sora@tiscali.it>, 2010, 2020.
msgid ""
msgstr ""
"Project-Id-Version: \n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2022-09-14 00:46+0000\n"
"PO-Revision-Date: 2020-11-12 23:44+0100\n"
"Last-Translator: Giovanni Sora <g.sora@tiscali.it>\n"
"Language-Team: Interlingua <kde-i18n-doc@kde.org>\n"
"Language: ia\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Lokalize 2.0\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"

#, kde-format
msgctxt "NAME OF TRANSLATORS"
msgid "Your names"
msgstr "Giovanni Sora"

#, kde-format
msgctxt "EMAIL OF TRANSLATORS"
msgid "Your emails"
msgstr "g.sora@tiscali.it"

#: networkmodel.cpp:159
#, kde-format
msgctxt "@item:intable Network type"
msgid "Point to Point"
msgstr "Puncto a Puncto"

#: networkmodel.cpp:166
#, kde-format
msgctxt "@item:intable Network type"
msgid "Broadcast"
msgstr "Broadcast"

#: networkmodel.cpp:173
#, kde-format
msgctxt "@item:intable Network type"
msgid "Multicast"
msgstr "Multicast"

#: networkmodel.cpp:180
#, kde-format
msgctxt "@item:intable Network type"
msgid "Loopback"
msgstr "Loopback"

#: nic.cpp:21
#, kde-format
msgid "kcm_nic"
msgstr "kcm_nic"

#: nic.cpp:22
#, kde-format
msgctxt "@title:window"
msgid "Network Interfaces"
msgstr "Interfacies de rete"

#: nic.cpp:26
#, kde-format
msgctxt "@info"
msgid "(c) 2001 - 2002 Alexander Neundorf"
msgstr "(c)2001 - 2002 Alexander Neundorf"

#: nic.cpp:28
#, kde-format
msgctxt "@info:credit"
msgid "Alexander Neundorf"
msgstr "Alexander Neundorf"

#: nic.cpp:28
#, kde-format
msgctxt "@info:credit"
msgid "creator"
msgstr "creator"

#: nic.cpp:29
#, kde-format
msgctxt "@info:credit"
msgid "Carl Schwan"
msgstr "Carl Schwan"

#: nic.cpp:29
#, kde-format
msgctxt "@info:credit"
msgid "developer"
msgstr "Developpator"

#: package/contents/ui/main.qml:15
#, kde-format
msgid "Network Interfaces"
msgstr "Interfacies de rete"

#: package/contents/ui/main.qml:30
#, kde-format
msgctxt "@label"
msgid "Name:"
msgstr "Nomine:"

#: package/contents/ui/main.qml:34
#, kde-format
msgctxt "@label"
msgid "Address:"
msgstr "Adresse:"

#: package/contents/ui/main.qml:38
#, kde-format
msgctxt "@label"
msgid "Network Mask:"
msgstr "Mascara de rete:"

#: package/contents/ui/main.qml:42
#, kde-format
msgctxt "@label"
msgid "Type:"
msgstr "Typo:"

#: package/contents/ui/main.qml:46
#, kde-format
msgctxt "@label"
msgid "Hardware Address:"
msgstr "Adresse Hardware:"

#: package/contents/ui/main.qml:51
#, kde-format
msgctxt "@label"
msgid "State:"
msgstr "Stato:"

#: package/contents/ui/main.qml:59
#, kde-format
msgctxt "State of network card is connected"
msgid "Up"
msgstr "In alto"

#: package/contents/ui/main.qml:60
#, kde-format
msgctxt "State of network card is disconnected"
msgid "Down"
msgstr "A basso"

#: package/contents/ui/main.qml:76
#, kde-format
msgctxt "@action:button"
msgid "Refresh"
msgstr "Refresca"

#~ msgctxt "@item:intext Mode of network card"
#~ msgid "Broadcast"
#~ msgstr "Broadcast"

#~ msgctxt "@item:intext Mode of network card"
#~ msgid "Point to Point"
#~ msgstr "Puncto a Puncto"

#~ msgctxt "@item:intext Mode of network card"
#~ msgid "Multicast"
#~ msgstr "Multicast"

#~ msgctxt "@item:intext Mode of network card"
#~ msgid "Loopback"
#~ msgstr "Loopback"

#~ msgctxt "@item:intext Mode of network card"
#~ msgid "Unknown"
#~ msgstr "Incognite"

#~ msgctxt "Unknown network mask"
#~ msgid "Unknown"
#~ msgstr "Incognite"

#~ msgctxt "Unknown HWaddr"
#~ msgid "Unknown"
#~ msgstr "Incognite"

#, fuzzy
#~| msgid "Broadcast"
#~ msgctxt "@item:intable Netork type"
#~ msgid "Broadcast"
#~ msgstr "Broadcast"

#~ msgid "HWAddr"
#~ msgstr "HWAdres"

#~ msgid "&Update"
#~ msgstr "Act&ualisa"

#~ msgid "kcminfo"
#~ msgstr "kcminfo"

#~ msgid "System Information Control Module"
#~ msgstr "Modulo de controlo de information de systema de KDE"
