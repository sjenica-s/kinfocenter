# Translation of kcmusb.po into Serbian.
# Tiron Andric <tiron@beotel.yu>, 2003.
# Toplica Tanaskovic <toptan@kde.org.yu>, 2003.
# Chusslove Illich <caslav.ilic@gmx.net>, 2005, 2007, 2008, 2012, 2016.
# Dalibor Djuric <dalibor.djuric@mozilla-srbija.org>, 2009, 2010.
msgid ""
msgstr ""
"Project-Id-Version: kcmusb\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2020-06-10 02:22+0200\n"
"PO-Revision-Date: 2016-03-13 14:33+0100\n"
"Last-Translator: Chusslove Illich <caslav.ilic@gmx.net>\n"
"Language-Team: Serbian <kde-i18n-sr@kde.org>\n"
"Language: sr@ijekavian\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=n==1 ? 3 : n%10==1 && n%100!=11 ? 0 : n"
"%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;\n"
"X-Accelerator-Marker: &\n"
"X-Text-Markup: kde4\n"
"X-Environment: kde\n"

#, kde-format
msgctxt "NAME OF TRANSLATORS"
msgid "Your names"
msgstr "Тирон Андрић,Часлав Илић"

#, kde-format
msgctxt "EMAIL OF TRANSLATORS"
msgid "Your emails"
msgstr "tiron@beotel.yu,caslav.ilic@gmx.net"

#: classes.i18n:1
#, kde-format
msgid "AT-commands"
msgstr "АТ‑наредбе"

#: classes.i18n:2
#, kde-format
msgid "ATM Networking"
msgstr "АТМ умрежавање"

#: classes.i18n:3
#, kde-format
msgid "Abstract (modem)"
msgstr "апстрактни (модем)"

#: classes.i18n:4
#, kde-format
msgid "Audio"
msgstr "аудио"

#: classes.i18n:5
#, kde-format
msgid "Bidirectional"
msgstr "двосмијерни"

#: classes.i18n:6
#, kde-format
msgid "Boot Interface Subclass"
msgstr "поткласа подизног сучеља"

#: classes.i18n:7
#, kde-format
msgid "Bulk (Zip)"
msgstr "количина (зип)"

#: classes.i18n:8
#, kde-format
msgid "CAPI 2.0"
msgstr "ЦАПИ 2.0"

#: classes.i18n:9
#, kde-format
msgid "CAPI Control"
msgstr "ЦАПИ контрола"

#: classes.i18n:10
#, kde-format
msgid "CDC PUF"
msgstr "ЦДЦ ПУФ"

#: classes.i18n:11
#, kde-format
msgid "Communications"
msgstr "комуникације"

#: classes.i18n:12
#, kde-format
msgid "Control Device"
msgstr "контролни уређај"

#: classes.i18n:13
#, kde-format
msgid "Control/Bulk"
msgstr "контрола/количински"

#: classes.i18n:14
#, kde-format
msgid "Control/Bulk/Interrupt"
msgstr "контрола/количински/прекид"

#: classes.i18n:15
#, kde-format
msgid "Data"
msgstr "подаци"

# skip-rule: t-line
#: classes.i18n:16
#, kde-format
msgid "Direct Line"
msgstr "директна веза"

#: classes.i18n:17
#, kde-format
msgid "Ethernet Networking"
msgstr "етернетско умрежавање"

#: classes.i18n:18
#, kde-format
msgid "Floppy"
msgstr "флопи"

#: classes.i18n:19
#, kde-format
msgid "HDLC"
msgstr "ХДЛЦ"

#: classes.i18n:20
#, kde-format
msgid "Host Based Driver"
msgstr "драјвер у домаћину"

#: classes.i18n:21
#, kde-format
msgid "Hub"
msgstr "чвориште"

#: classes.i18n:22
#, kde-format
msgid "Human Interface Devices"
msgstr "уређаји људског сучеља"

#: classes.i18n:23
#, kde-format
msgid "I.430 ISDN BRI"
msgstr "И.430 ИСДН БРИ"

#: classes.i18n:24
#, kde-format
msgid "Interface"
msgstr "сучеље"

#: classes.i18n:25
#, kde-format
msgid "Keyboard"
msgstr "тастатура"

#: classes.i18n:26
#, kde-format
msgid "Mass Storage"
msgstr "масовно складиште"

#: classes.i18n:27
#, kde-format
msgid "Mouse"
msgstr "миш"

#: classes.i18n:28
#, kde-format
msgid "Multi-Channel"
msgstr "вишеканалски"

#: classes.i18n:29
#, kde-format
msgid "No Subclass"
msgstr "без поткласе"

#: classes.i18n:30
#, kde-format
msgid "Non Streaming"
msgstr "без струјања"

#: classes.i18n:31
#, kde-format
msgid "None"
msgstr "ниједан"

#: classes.i18n:32
#, kde-format
msgid "Printer"
msgstr "штампач"

#: classes.i18n:33
#, kde-format
msgid "Q.921"
msgstr "Ку.921"

#: classes.i18n:34
#, kde-format
msgid "Q.921M"
msgstr "Ку.921М"

#: classes.i18n:35
#, kde-format
msgid "Q.921TM"
msgstr "Ку.921ТМ"

#: classes.i18n:36
#, kde-format
msgid "Q.932 EuroISDN"
msgstr "Ку.932 (евроИСДН)"

#: classes.i18n:37
#, kde-format
msgid "SCSI"
msgstr "скази"

#: classes.i18n:38
#, kde-format
msgid "Streaming"
msgstr "струјање"

#: classes.i18n:39
#, kde-format
msgid "Telephone"
msgstr "телефон"

#: classes.i18n:40
#, kde-format
msgid "Transparent"
msgstr "прозиран"

#: classes.i18n:41
#, kde-format
msgid "Unidirectional"
msgstr "једносмјеран"

#: classes.i18n:42
#, kde-format
msgid "V.120 V.24 rate ISDN"
msgstr "В.120 В.24 ИСДН"

#: classes.i18n:43
#, kde-format
msgid "V.42bis"
msgstr "В.42бис"

#: classes.i18n:44
#, kde-format
msgid "Vendor Specific"
msgstr "посебно произвођача"

#: classes.i18n:45
#, kde-format
msgid "Vendor Specific Class"
msgstr "посебна класа произвођача"

#: classes.i18n:46
#, kde-format
msgid "Vendor Specific Protocol"
msgstr "посебан протокол произвођача"

#: classes.i18n:47
#, kde-format
msgid "Vendor Specific Subclass"
msgstr "посебна поткласа произвођача"

#: classes.i18n:48
#, kde-format
msgid "Vendor specific"
msgstr "посебно произвођача"

#: kcmusb.cpp:35
#, kde-format
msgid "This module allows you to see the devices attached to your USB bus(es)."
msgstr "Овај модул даје преглед уређаја прикачених на УСБ магистралу."

#: kcmusb.cpp:49
#, kde-format
msgid "Device"
msgstr "Уређај"

#: kcmusb.cpp:69
#, kde-format
msgid "kcmusb"
msgstr "КЦМ‑УСБ "

#: kcmusb.cpp:69
#, kde-format
msgid "USB Devices"
msgstr "УСБ уређаји"

#: kcmusb.cpp:71
#, kde-format
msgid "(c) 2001 Matthias Hoelzer-Kluepfel"
msgstr "© 2001, Матијас Хелцер-Клипфел"

#: kcmusb.cpp:73
#, kde-format
msgid "Matthias Hoelzer-Kluepfel"
msgstr "Матијас Хелцер-Клипфел"

#: kcmusb.cpp:74
#, kde-format
msgid "Leo Savernik"
msgstr "Лео Саверник"

#: kcmusb.cpp:74
#, kde-format
msgid "Live Monitoring of USB Bus"
msgstr "Уживо надгледање УСБ магистрале"

# >> @item:intext USB product
#: usbdevices.cpp:147
#, kde-format
msgid "Unknown"
msgstr "непознат"

#: usbdevices.cpp:156
#, kde-format
msgid "<b>Manufacturer:</b> "
msgstr "<b>Произвођач:</b> "

#: usbdevices.cpp:158
#, kde-format
msgid "<b>Serial #:</b> "
msgstr "<b>Серијски број:</b> "

#: usbdevices.cpp:166
#, kde-format
msgid "<tr><td><i>Class</i></td>%1</tr>"
msgstr "<tr><td><i>класа</i></td><td>%1</td></tr>"

#: usbdevices.cpp:171
#, kde-format
msgid "<tr><td><i>Subclass</i></td>%1</tr>"
msgstr "<tr><td><i>поткласа</i></td><td>%1</td></tr>"

#: usbdevices.cpp:176
#, kde-format
msgid "<tr><td><i>Protocol</i></td>%1</tr>"
msgstr "<tr><td><i>протокол</i></td><td>%1</td></tr>"

#: usbdevices.cpp:178
#, kde-format
msgid "<tr><td><i>USB Version</i></td><td>%1.%2</td></tr>"
msgstr "<tr><td><i>УСБ верзија</i></td><td>%1.%2</td></tr>"

#: usbdevices.cpp:188
#, kde-format
msgid "<tr><td><i>Vendor ID</i></td><td>0x%1</td></tr>"
msgstr "<tr><td><i>ИД произвођача</i></td><td>0x%1</td></tr>"

#: usbdevices.cpp:193
#, kde-format
msgid "<tr><td><i>Product ID</i></td><td>0x%1</td></tr>"
msgstr "<tr><td><i>ИД производа</i></td><td>0x%1</td></tr>"

#: usbdevices.cpp:194
#, kde-format
msgid "<tr><td><i>Revision</i></td><td>%1.%2</td></tr>"
msgstr "<tr><td><i>ревизија</i></td><td>%1.%2</td></tr>"

#: usbdevices.cpp:199
#, kde-format
msgid "<tr><td><i>Speed</i></td><td>%1 Mbit/s</td></tr>"
msgstr "<tr><td><i>брзина</i></td><td>%1 Mbit/s</td></tr>"

#: usbdevices.cpp:200
#, kde-format
msgid "<tr><td><i>Channels</i></td><td>%1</td></tr>"
msgstr "<tr><td><i>канала</i></td><td>%1</td></tr>"

#: usbdevices.cpp:203
#, kde-format
msgid "<tr><td><i>Power Consumption</i></td><td>%1 mA</td></tr>"
msgstr "<tr><td><i>потрошња енергије</i></td><td>%1 mA</td></tr>"

#: usbdevices.cpp:205
#, kde-format
msgid "<tr><td><i>Power Consumption</i></td><td>self powered</td></tr>"
msgstr "<tr><td><i>потрошња енергије</i></td><td>самонапајање</td></tr>"

#: usbdevices.cpp:206
#, kde-format
msgid "<tr><td><i>Attached Devicenodes</i></td><td>%1</td></tr>"
msgstr "<tr><td><i>прикачени чворови</i></td><td>%1</td></tr>"

#: usbdevices.cpp:214
#, kde-format
msgid "<tr><td><i>Max. Packet Size</i></td><td>%1</td></tr>"
msgstr "<tr><td><i>макс. величина пакета</i></td><td>%1</td></tr>"

#: usbdevices.cpp:219
#, kde-format
msgid "<tr><td><i>Bandwidth</i></td><td>%1 of %2 (%3%)</td></tr>"
msgstr "<tr><td><i>пропусност</i></td><td>%1 од %2 (%3%)</td></tr>"

#: usbdevices.cpp:220
#, kde-format
msgid "<tr><td><i>Intr. requests</i></td><td>%1</td></tr>"
msgstr "<tr><td><i>прекидних захтјева</i></td><td>%1</td></tr>"

#: usbdevices.cpp:221
#, kde-format
msgid "<tr><td><i>Isochr. requests</i></td><td>%1</td></tr>"
msgstr "<tr><td><i>изохроних захтјева</i></td><td>%1</td></tr>"

#: usbdevices.cpp:427
#, kde-format
msgid ""
"Could not open one or more USB controller. Make sure, you have read access "
"to all USB controllers that should be listed here."
msgstr ""
"Не могу да отворим један или више УСБ контролора. Провјерите имате ли "
"приступ за читање за све УСБ контролоре које овдје треба набројати."
