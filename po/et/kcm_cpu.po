# Copyright (C) YEAR This file is copyright:
# This file is distributed under the same license as the kinfocenter package.
#
# Mihkel Tõnnov <mihhkel@gmail.com>, 2022.
msgid ""
msgstr ""
"Project-Id-Version: kinfocenter\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2021-11-17 00:29+0000\n"
"PO-Revision-Date: 2022-11-17 23:31+0100\n"
"Last-Translator: Mihkel Tõnnov <mihhkel@gmail.com>\n"
"Language-Team: Estonian <>\n"
"Language: et\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 22.08.3\n"

#, kde-format
msgctxt "NAME OF TRANSLATORS"
msgid "Your names"
msgstr "Mihkel Tõnnov"

#, kde-format
msgctxt "EMAIL OF TRANSLATORS"
msgid "Your emails"
msgstr "mihhkel@gmail.com"

#: main.cpp:23
#, kde-format
msgctxt "@label kcm name"
msgid "CPU"
msgstr "Protsessor"

#: main.cpp:24
#, kde-format
msgid "Harald Sitter"
msgstr "Harald Sitter"

#: package/contents/ui/main.qml:14
msgctxt "@info:whatsthis"
msgid "Advanced CPU Information"
msgstr "Protsessori lisateave"
