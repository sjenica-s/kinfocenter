# translation of kcmnic.po to Hindi
# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: kcmnic\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2022-09-14 00:46+0000\n"
"PO-Revision-Date: 2010-12-09 01:20+0530\n"
"Last-Translator: G Karunakar\n"
"Language-Team: Hindi <en@li.org>\n"
"Language: hi\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: KBabel 1.11.4\n"
"Plural-Forms: nplurals=2; plural=(n!=1);\n"

#, kde-format
msgctxt "NAME OF TRANSLATORS"
msgid "Your names"
msgstr "जी करूणाकर"

#, kde-format
msgctxt "EMAIL OF TRANSLATORS"
msgid "Your emails"
msgstr "karunakar@indlinux.org"

#: networkmodel.cpp:159
#, fuzzy, kde-format
#| msgid "Point to Point"
msgctxt "@item:intable Network type"
msgid "Point to Point"
msgstr "बिन्दु से बिन्दु"

#: networkmodel.cpp:166
#, fuzzy, kde-format
#| msgctxt "@item:intext Mode of network card"
#| msgid "Broadcast"
msgctxt "@item:intable Network type"
msgid "Broadcast"
msgstr "ब्रॉडकास्ट"

#: networkmodel.cpp:173
#, fuzzy, kde-format
#| msgid "Multicast"
msgctxt "@item:intable Network type"
msgid "Multicast"
msgstr "मल्टीकास्ट"

#: networkmodel.cpp:180
#, fuzzy, kde-format
#| msgid "Loopback"
msgctxt "@item:intable Network type"
msgid "Loopback"
msgstr "लूपबैक"

#: nic.cpp:21
#, kde-format
msgid "kcm_nic"
msgstr ""

#: nic.cpp:22
#, fuzzy, kde-format
#| msgid "Network Mask"
msgctxt "@title:window"
msgid "Network Interfaces"
msgstr "नेटवर्क मास्क"

#: nic.cpp:26
#, fuzzy, kde-format
#| msgid "(c) 2001 - 2002 Alexander Neundorf"
msgctxt "@info"
msgid "(c) 2001 - 2002 Alexander Neundorf"
msgstr "(c) 2001 - 2002 अलेक्ज़ेंडर न्यून्दॉर्फ"

#: nic.cpp:28
#, fuzzy, kde-format
#| msgid "Alexander Neundorf"
msgctxt "@info:credit"
msgid "Alexander Neundorf"
msgstr "अलेक्ज़ेंडर न्यून्दॉर्फ"

#: nic.cpp:28
#, kde-format
msgctxt "@info:credit"
msgid "creator"
msgstr ""

#: nic.cpp:29
#, kde-format
msgctxt "@info:credit"
msgid "Carl Schwan"
msgstr ""

#: nic.cpp:29
#, kde-format
msgctxt "@info:credit"
msgid "developer"
msgstr ""

#: package/contents/ui/main.qml:15
#, fuzzy, kde-format
#| msgid "Network Mask"
msgid "Network Interfaces"
msgstr "नेटवर्क मास्क"

#: package/contents/ui/main.qml:30
#, fuzzy, kde-format
#| msgid "Name"
msgctxt "@label"
msgid "Name:"
msgstr "नाम"

#: package/contents/ui/main.qml:34
#, fuzzy, kde-format
#| msgid "IP Address"
msgctxt "@label"
msgid "Address:"
msgstr "आईपी पता"

#: package/contents/ui/main.qml:38
#, fuzzy, kde-format
#| msgid "Network Mask"
msgctxt "@label"
msgid "Network Mask:"
msgstr "नेटवर्क मास्क"

#: package/contents/ui/main.qml:42
#, fuzzy, kde-format
#| msgid "Type"
msgctxt "@label"
msgid "Type:"
msgstr "क़िस्म"

#: package/contents/ui/main.qml:46
#, kde-format
msgctxt "@label"
msgid "Hardware Address:"
msgstr ""

#: package/contents/ui/main.qml:51
#, fuzzy, kde-format
#| msgid "State"
msgctxt "@label"
msgid "State:"
msgstr "स्थिति"

#: package/contents/ui/main.qml:59
#, kde-format
msgctxt "State of network card is connected"
msgid "Up"
msgstr "ऊपर"

#: package/contents/ui/main.qml:60
#, kde-format
msgctxt "State of network card is disconnected"
msgid "Down"
msgstr "नीचे"

#: package/contents/ui/main.qml:76
#, kde-format
msgctxt "@action:button"
msgid "Refresh"
msgstr ""

#~ msgctxt "@item:intext Mode of network card"
#~ msgid "Broadcast"
#~ msgstr "ब्रॉडकास्ट"

#~ msgctxt "@item:intext Mode of network card"
#~ msgid "Point to Point"
#~ msgstr "बिन्दु से बिन्दु"

#~ msgctxt "@item:intext Mode of network card"
#~ msgid "Multicast"
#~ msgstr "मल्टीकास्ट"

#~ msgctxt "@item:intext Mode of network card"
#~ msgid "Loopback"
#~ msgstr "लूपबैक"

#~ msgctxt "@item:intext Mode of network card"
#~ msgid "Unknown"
#~ msgstr "अज्ञात"

#~ msgctxt "Unknown network mask"
#~ msgid "Unknown"
#~ msgstr "अज्ञात"

#~ msgctxt "Unknown HWaddr"
#~ msgid "Unknown"
#~ msgstr "अज्ञात"

#, fuzzy
#~| msgid "Broadcast"
#~ msgctxt "@item:intable Netork type"
#~ msgid "Broadcast"
#~ msgstr "ब्रॉडकास्ट"

#~ msgid "HWAddr"
#~ msgstr "HWAddr"

#~ msgid "&Update"
#~ msgstr "अद्यतन (&U)"

#~ msgid "kcminfo"
#~ msgstr "kcminfo"

#~ msgid "System Information Control Module"
#~ msgstr "तंत्र जानकारी नियंत्रण इकाई"
